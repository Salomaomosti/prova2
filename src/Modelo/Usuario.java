package Modelo;

public class Usuario extends Pessoa{
	
	private String cpf;
	
	public Usuario(String nome,String email,String contato,String cpf){
		super(nome,email,contato);
		this.cpf = cpf;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	

}
