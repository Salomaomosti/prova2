package Modelo;

import java.util.ArrayList;

public class Participacao {

		private String tema;
		private String post;
		private String comentario;
		private Pessoa pessoa;
		private ArrayList<Participacao> lista = new ArrayList<Participacao>(); 
		
		public Participacao(String tema, String post,Pessoa pessoa){
			this.tema = tema;
			this.post = post;
			this.pessoa = pessoa;
		}
		
		public String getTema() {
			return tema;
		}
		public void setTema(String tema) {
			this.tema = tema;
		}
		public String getPost() {
			return post;
		}
		public void setPost(String post) {
			this.post = post;
		}
		public String getComentario() {
			return comentario;
		}
		public void setComentario(String comentario) {
			this.comentario = comentario;
		}
		public Pessoa getPessoa() {
			return pessoa;
		}
		public void setPessoa(Pessoa pessoa) {
			this.pessoa = pessoa;
		}
		


	public void criar(Participacao umaParticipacao){
		lista.add(umaParticipacao);
		System.out.println("Enviado");
	}
		
	}